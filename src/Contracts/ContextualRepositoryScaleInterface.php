<?php

namespace Laudis\Scale\Contracts;

use DateTimeInterface;
use Laudis\Scale\Exceptions\ScaleItemNotFoundException;

interface ContextualRepositoryScaleInterface
{
    /**
     * Gets the value with a given context.
     * @param DateTimeInterface $dateTime
     * @param string $identifier
     * @return ScaleInterface
     *
     * @throws ScaleItemNotFoundException
     */
    public function getFromDate(DateTimeInterface $dateTime, string $identifier): ScaleInterface;
}
