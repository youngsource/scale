<?php

namespace Laudis\Scale\Contracts;

/**
 * Interface ScaleRuleInterface
 * Represents a rule in of the scale.
 */
interface ScaleRuleInterface
{
    /**
     * Returns the trigger value of this scale rule.
     *
     * @return int|float
     */
    public function getFrom();

    /**
     * Returns the right hand side of the scale operator.
     *
     * @return int|float
     */
    public function getRhs();
}
