<?php

namespace Laudis\Scale\Contracts;

interface ScaleInterface
{
    /**
     * Adds a rule to the scale.
     *
     * @param ScaleRuleInterface $rule
     */
    public function addScaleRule(ScaleRuleInterface $rule): void;

    /**
     * Calculates the result of the scale from the given value.
     *
     * @param int|float $value
     * @return int|float
     */
    public function calculate($value);

    /**
     * Returns all the rules in the scale, sorted by their threshold values.
     *
     * @return ScaleRuleInterface[]
     */
    public function getScaleRules(): array;

    /**
     * @return ScaleOperatorInterface
     */
    public function getOperator(): ScaleOperatorInterface;
}
