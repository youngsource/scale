<?php

namespace Laudis\Scale\Contracts;

/**
 * Interface ScaleOperatorInterface
 * Represents the operation that takes place
 */
interface ScaleOperatorInterface
{
    /**
     * @param int|float $lhs
     * @param int|float $rhs
     * @return int|float
     */
    public function operate($lhs, $rhs);

    /**
     * Returns the descriptions of the operator.
     *
     * @return string
     */
    public function getDescription(): string;
}
