<?php

namespace Laudis\Scale\Contracts;

use Laudis\Scale\Exceptions\ScaleItemNotFoundException;

interface ScaleRepositoryInterface
{
    /**
     * @param string $identifier
     * @return ScaleInterface
     *
     * @throws ScaleItemNotFoundException
     */
    public function get(string $identifier): ScaleInterface;
}
