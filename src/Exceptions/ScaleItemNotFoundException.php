<?php

namespace Laudis\Scale\Exceptions;

use RuntimeException;

/**
 * Class ScaleItemNotFoundException
 */
class ScaleItemNotFoundException extends RuntimeException
{

}
