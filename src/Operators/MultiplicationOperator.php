<?php

namespace Laudis\Scale\Operators;

use Laudis\Scale\Contracts\ScaleOperatorInterface;

final class MultiplicationOperator implements ScaleOperatorInterface
{
    /**
     * @param int|float $lhs
     * @param int|float $rhs
     * @return int|float
     */
    public function operate($lhs, $rhs)
    {
        return $lhs * $rhs;
    }

    public function getDescription(): string
    {
        return '&times;';
    }
}
