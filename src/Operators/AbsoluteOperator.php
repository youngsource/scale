<?php
declare(strict_types=1);

namespace Laudis\Scale\Operators;

use Laudis\Scale\Contracts\ScaleOperatorInterface;

/**
 * Class DegressiveOperator
 * @package Laudis\Calculators\VoordeelWagen
 */
final class AbsoluteOperator implements ScaleOperatorInterface
{
    /**
     * @param int|float $lhs
     * @param int|float $rhs
     * @return int|float
     */
    public function operate($lhs, $rhs)
    {
        return $rhs;
    }

    /**
     * Returns the descriptions of the operator.
     *
     * @return string
     */
    public function getDescription(): string
    {
        return 'Absolute operator';
    }
}
