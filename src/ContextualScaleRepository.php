<?php

namespace Laudis\Scale;

use DateTimeInterface;
use Laudis\Scale\Contracts\ContextualRepositoryScaleInterface;
use Laudis\Scale\Contracts\ScaleInterface;
use Laudis\Scale\Exceptions\ScaleItemNotFoundException;
use function ksort;

final class ContextualScaleRepository implements ContextualRepositoryScaleInterface
{
    private $mappings = [];

    public function addScale(string $identifier, DateTimeInterface $dateTime, ScaleInterface $scale): void
    {
        if (!isset($this->mappings[$identifier])) {
            $this->mappings[$identifier] = [];
        }
        $this->mappings[$identifier][$dateTime->getTimestamp()] = $scale;
    }

    /**
     * Gets the value with a given context.
     * @param DateTimeInterface $dateTime
     * @param string $identifier
     * @return ScaleInterface
     */
    public function getFromDate(DateTimeInterface $dateTime, string $identifier): ScaleInterface
    {
        $current = $this->findScale($dateTime, $identifier);
        if ($current === null) {
            throw new ScaleItemNotFoundException('Cannot find scale item: ' . $identifier);
        }
        return $current;
    }

    /**
     * @param DateTimeInterface $dateTime
     * @param string $identifier
     * @return ScaleInterface|null
     */
    private function findScale(DateTimeInterface $dateTime, string $identifier): ?ScaleInterface
    {
        if (!isset($this->mappings[$identifier])) {
            return null;
        }

        $set = $this->mappings[$identifier];
        ksort($set);
        $time = $dateTime->getTimestamp();
        $current = null;
        foreach ($set as $key => $value) {
            if ($time < $key) {
                break;
            }
            $current = $value;
        }

        return $current;
    }
}
