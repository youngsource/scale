<?php

namespace Laudis\Scale;

use ArrayIterator;
use Laudis\Scale\Contracts\ScaleInterface;
use Laudis\Scale\Contracts\ScaleOperatorInterface;
use Laudis\Scale\Contracts\ScaleRuleInterface;
use const PHP_FLOAT_MAX;
use function usort;

final class Scale implements ScaleInterface
{
    /** @var ScaleOperatorInterface */
    private $operator;
    /** @var ScaleRuleInterface[] */
    private $scaleRules = [];
    /** @var bool */
    private $sorted = true;

    public function __construct(ScaleOperatorInterface $operator)
    {
        $this->operator = $operator;
    }

    /**
     * @param ScaleRuleInterface $rule
     */
    public function addScaleRule(ScaleRuleInterface $rule): void
    {
        $this->sorted = false;
        $this->scaleRules[] = $rule;
    }

    /**
     * @param int|float $value
     * @return int|float
     */
    public function calculate($value)
    {
        $this->sortRules();
        $tbr = 0 * $value; // Cast the zero to int or float, depending on the value.
        $iterator = new ArrayIterator($this->scaleRules);
        while ($iterator->valid()) {
            /** @var ScaleRuleInterface $current */
            $current = $iterator->current();
            if ($current->getFrom() > $value) {
                break;
            }
            $iterator->next();
            if ($iterator->valid()) {
                $next = $iterator->current();
                $limitedValue = min($this->normalizeValue($value, $current), $next->getFrom() - $current->getFrom());
                $tbr += $this->operator->operate($limitedValue, $current->getRhs());
            } else {
                $tbr += $this->operator->operate($this->normalizeValue($value, $current), $current->getRhs());
            }
        }
        return $tbr;
    }

    /**
     * @return array
     */
    public function getScaleRules(): array
    {
        $this->sortRules();
        return $this->scaleRules;
    }

    /**
     * @param int|float $value
     * @param ScaleRuleInterface $current
     * @return mixed
     */
    private function normalizeValue($value, ScaleRuleInterface $current)
    {
        // Cast the zero to float or int, depending on the value.
        return max(0 * $value, $value - $current->getFrom());
    }

    private function sortRules(): void
    {
        if (!$this->sorted) {
            usort($this->scaleRules, function (ScaleRuleInterface $x, ScaleRuleInterface $y) {
                return $x->getFrom() - $y->getFrom();
            });
            $this->sorted = true;
        }
    }

    /**
     * @return ScaleOperatorInterface
     */
    public function getOperator(): ScaleOperatorInterface
    {
        return $this->operator;
    }
}
