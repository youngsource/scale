<?php

namespace Laudis\Scale;

use Laudis\Scale\Contracts\ScaleInterface;
use Laudis\Scale\Contracts\ScaleRepositoryInterface;
use Laudis\Scale\Exceptions\ScaleItemNotFoundException;

/**
 * Class ScaleRepository
 * @package Laudis\Scale
 */
final class ScaleRepository implements ScaleRepositoryInterface
{
    /** @var array<ScaleInterface[], callable():ScaleInterface> */
    private $scales = [];

    /**
     * @param string $identifier
     * @param callable():ScaleInterface|ScaleInterface $scale
     */
    public function register(string $identifier, $scale): void
    {
        $this->scales[$identifier] = $scale;
    }

    /**
     * @param string $identifier
     * @return ScaleInterface
     */
    public function get(string $identifier): ScaleInterface
    {
        if (!isset($this->scales[$identifier])) {
            throw new ScaleItemNotFoundException('Could not find scale with id: '. $identifier);
        }
        $scale = $this->scales[$identifier];
        if ($scale instanceof ScaleInterface) {
            return $scale;
        }
        return $scale();
    }
}
