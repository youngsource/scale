<?php

namespace Laudis\Scale;

use Laudis\Scale\Contracts\ScaleInterface;
use const PHP_FLOAT_MAX;

/**
 * Class ScalePresenter
 * @package Laudis\Scale
 */
final class ScalePresenter
{
    /**
     * @var ScaleInterface
     */
    private $scale;

    /**
     * ScalePresenter constructor.
     * @param ScaleInterface $scale
     */
    public function __construct(ScaleInterface $scale)
    {
        $this->scale = $scale;
    }

    /**
     * Presents the scale and it's calculation logic.
     *
     * @param float $value
     * @return array
     */
    public function present($value = PHP_FLOAT_MAX): array
    {
        $aggregated = 0;
        $rules = $this->scale->getScaleRules();
        $tbr = [];

        foreach ($rules as $i => $currentRule) {
            $nextRule = $rules[$i + 1] ?? null;

            $start = $currentRule->getFrom();
            $end = $nextRule === null ? null : $nextRule->getFrom();
            $lhs = $this->calculateLhs($value, $end, $start);
            $rhs = $currentRule->getRhs();
            $tbr[] = $this->presentRule($start, $lhs, $rhs, $aggregated, $value > $start, $end);
            $aggregated += $lhs * $rhs;
        }
        return [
            'input' => $value,
            'output' => $this->scale->calculate($value),
            'explanation' => $tbr
        ];
    }

    /**
     * @param $value
     * @param $end
     * @param $start
     * @return float
     */
    private function calculateLhs($value, $end, $start): float
    {
        $end = $end ?? PHP_FLOAT_MAX;
        if ($value < $start) {
            return 0;
        }

        if ($end > $value && $value >= $start) {
            $lhs = $value - $start;
        } else {
            $lhs = $end - $start;
        }
        return $lhs;
    }

    /**
     * @param int|float $start
     * @param int|float $lhs
     * @param int|float $rhs
     * @param int|float $aggregated
     * @param bool $valueInRule
     * @param int|float|null $end
     * @return array
     */
    private function presentRule($start, $lhs, $rhs, $aggregated, bool $valueInRule, $end = null): array
    {
        return [
            'start' => $start,
            'end' => $end,
            'operation' => $this->scale->getOperator()->getDescription(),
            'rhs' => $rhs,
            'lhs' => $lhs,
            'ruleValue' => $lhs * $rhs,
            'aggregated' => $aggregated,
            'valueInRule' => $valueInRule,
            'aggregatedPlusCurrent' => $lhs * $rhs + $aggregated
        ];
    }
}
