<?php

namespace Laudis\Scale;

use Laudis\Scale\Contracts\ScaleRuleInterface;

final class ScaleRule implements ScaleRuleInterface
{
    /** @var int|float */
    private $from;
    /** @var int|float */
    private $rhs;

    public function __construct($from, $rhs)
    {
        $this->from = $from;
        $this->rhs = $rhs;
    }

    /**
     * @param int|float $from
     * @param int|float $rhs
     * @return ScaleRule
     */
    public static function make($from, $rhs): ScaleRule
    {
        return new self($from, $rhs);
    }

    /**
     * Returns the trigger value of this scale rule.
     *
     * @return int|float
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * Returns the right hand side of the scale operator.
     *
     * @return int|float
     */
    public function getRhs()
    {
        return $this->rhs;
    }
}
