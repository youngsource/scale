<?php

namespace Laudis\Scale\Testing;

use Laudis\Scale\Operators\AbsoluteOperator;
use Laudis\Scale\Contracts\ScaleInterface;
use Laudis\Scale\Operators\MultiplicationOperator;
use Laudis\Scale\Scale;
use Laudis\Scale\ScaleRule;
use PHPUnit\Framework\TestCase;

final class ScaleTest extends TestCase
{
    /** @var ScaleInterface */
    private $scale;

    protected function setUp(): void
    {
        parent::setUp();
        $this->scale = new Scale(new MultiplicationOperator);
        $this->scale->addScaleRule(ScaleRule::make(0, 2));
        $this->scale->addScaleRule(ScaleRule::make(2, 4));
        $this->scale->addScaleRule(ScaleRule::make(1, 8));
    }

    public function testCalculateSimple(): void
    {
        self::assertEquals(0, $this->scale->calculate(0));
        self::assertIsInt($this->scale->calculate(0));
        self::assertEquals(2, $this->scale->calculate(1));
        self::assertIsInt($this->scale->calculate(0));
        self::assertEquals(10, $this->scale->calculate(2));
        self::assertIsInt($this->scale->calculate(0));
        self::assertEquals(18, $this->scale->calculate(4));
        self::assertIsInt($this->scale->calculate(0));
    }

    public function testCalculateDoubles(): void
    {
        self::assertEquals(0, $this->scale->calculate(0.0));
        self::assertIsFloat($this->scale->calculate(0.0));
        self::assertEquals(6, $this->scale->calculate(1.5));
        self::assertIsFloat($this->scale->calculate(1.5));
    }

    public function testWithCeiling(): void
    {
        $this->scale->addScaleRule(ScaleRule::make(3, 0));
        self::assertEquals(14, $this->scale->calculate(4));
    }

    public function testAbsoluteTest(): void
    {
        $scale = new Scale(new AbsoluteOperator);
        $scale->addScaleRule(ScaleRule::make(0, 1));
        $scale->addScaleRule(ScaleRule::make(60, -0.1));
        $scale->addScaleRule(ScaleRule::make(106, -0.1));
        $scale->addScaleRule(ScaleRule::make(126, -0.05));
        $scale->addScaleRule(ScaleRule::make(156, -0.05));
        $scale->addScaleRule(ScaleRule::make(181, -0.1));
        $scale->addScaleRule(ScaleRule::make(206, -0.1));
        self::assertEquals($scale->calculate(106), 0.8);
        self::assertEquals($scale->calculate(0), 1);
        self::assertEquals($scale->calculate(50000), 0.5);
    }
}
