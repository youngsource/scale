<?php

namespace Laudis\Scale\Testing;

use Laudis\Scale\Operators\MultiplicationOperator;
use Laudis\Scale\Scale;
use Laudis\Scale\ScalePresenter;
use Laudis\Scale\ScaleRule;
use PHPUnit\Framework\TestCase;
use const PHP_FLOAT_MAX;

final class ScalePresenterTest extends TestCase
{
    /** @var ScalePresenter */
    private $presenter;

    public function testPresentUnlimited(): void
    {
        self::assertEquals([
            'input' => PHP_FLOAT_MAX,
            'output' => 'INF',
            'explanation' => [
                [
                    'start' => 0,
                    'end' => 1,
                    'operation' => '&times;',
                    'rhs' => 2,
                    'lhs' => 1,
                    'aggregated' => 0,
                    'valueInRule' => true,
                    'ruleValue' => 2.0,
                    'aggregatedPlusCurrent' => 2.0
                ],
                [
                    'start' => 1,
                    'end' => 2,
                    'operation' => '&times;',
                    'rhs' => 8,
                    'lhs' => 1,
                    'aggregated' => 2,
                    'valueInRule' => true,
                    'ruleValue' => 8.0,
                    'aggregatedPlusCurrent' => 2.0 + 8.0
                ],
                [
                    'start' => 2,
                    'end' => null,
                    'operation' => '&times;',
                    'rhs' => 4,
                    'lhs' => PHP_FLOAT_MAX - 2,
                    'aggregated' => 10,
                    'valueInRule' => true,
                    'ruleValue' => INF,
                    'aggregatedPlusCurrent' => INF
                ]
            ]
        ], $this->presenter->present());
    }

    public function testPresentLimited(): void
    {
        self::assertEquals([
            'input' => 2,
            'output' => 10,
            'explanation' => [
                [
                    'start' => 0,
                    'end' => 1,
                    'operation' => '&times;',
                    'rhs' => 2,
                    'lhs' => 1,
                    'aggregated' => 0,
                    'valueInRule' => true,
                    'ruleValue' => 2.0,
                    'aggregatedPlusCurrent' => 2.0
                ],
                [
                    'start' => 1,
                    'end' => 2,
                    'operation' => '&times;',
                    'rhs' => 8,
                    'lhs' => 1,
                    'aggregated' => 2,
                    'valueInRule' => true,
                    'ruleValue' => 8.0,
                    'aggregatedPlusCurrent' => 2.0 + 8.0
                ],
                [
                    'start' => 2,
                    'end' => null,
                    'operation' => '&times;',
                    'rhs' => 4,
                    'lhs' => 0,
                    'aggregated' => 10,
                    'valueInRule' => false,
                    'ruleValue' => 0.0,
                    'aggregatedPlusCurrent' =>  2.0 + 8.0 + 0
                ]
            ]
        ], $this->presenter->present(2));
    }

    public function testVenbScale(): void
    {
        $scale = new Scale(new MultiplicationOperator);
        $scale->addScaleRule(ScaleRule::make(0.0, 0.2498));
        $scale->addScaleRule(ScaleRule::make(25000.0, 0.3193));
        $scale->addScaleRule(ScaleRule::make(90000.0, 0.3554));
        $scale->addScaleRule(ScaleRule::make(322500.0, 0.0));
        $presenter = new ScalePresenter($scale);
        self::assertEquals([
            'input' => 300000,
            'output' => 101633.5,
            'explanation' => [
                [
                    'start' => 0,
                    'end' => 25000,
                    'operation' => '&times;',
                    'rhs' => 0.2498,
                    'lhs' => 25000,
                    'aggregated' => 0,
                    'valueInRule' => true,
                    'ruleValue' => 6245.0,
                    'aggregatedPlusCurrent' => 6245.0
                ],
                [
                    'start' => 25000,
                    'end' => 90000,
                    'operation' => '&times;',
                    'rhs' => 0.3193,
                    'lhs' => 65000,
                    'aggregated' => 6245.0,
                    'valueInRule' => true,
                    'ruleValue' => 20754.5,
                    'aggregatedPlusCurrent' => 6245.0 + 20754.5
                ],
                [
                    'start' => 90000,
                    'end' => 322500,
                    'operation' => '&times;',
                    'rhs' => 0.3554,
                    'lhs' => 210000,
                    'aggregated' => 26999.5,
                    'valueInRule' => true,
                    'ruleValue' => 74634.0,
                    'aggregatedPlusCurrent' => 6245.0 + 20754.5 + 74634.0
                ],
                [
                    'start' => 322500.0,
                    'end' => null,
                    'operation' => '&times;',
                    'rhs' => 0.0,
                    'lhs' => 0,
                    'aggregated' => 101633.5,
                    'valueInRule' => false,
                    'ruleValue' => 0.0,
                    'aggregatedPlusCurrent' => 6245.0 + 20754.5 + 74634.0 + 0.0
                ]
            ]
        ], $presenter->present(300000));
    }

    public function testVenbSimpleScale(): void
    {
        $scale = new Scale(new MultiplicationOperator);
        $scale->addScaleRule(ScaleRule::make(0, 0.2 * 1.02));
        $scale->addScaleRule(ScaleRule::make(100000, 0.29 * 1.02));
        $presenter = new ScalePresenter($scale);
        self::assertEquals([
            'input' => 700000,
            'output' => 177480.0 + 20400.0,
            'explanation' => [
                [
                    'start' => 0,
                    'end' => 100000,
                    'operation' => '&times;',
                    'rhs' => 0.20400000000000001,
                    'lhs' => 100000,
                    'aggregated' => 0,
                    'valueInRule' => true,
                    'ruleValue' => 20400.0,
                    'aggregatedPlusCurrent' => 20400.0
                ],
                [
                    'start' => 100000,
                    'end' => null,
                    'operation' => '&times;',
                    'rhs' => 0.2958,
                    'lhs' => 600000,
                    'aggregated' => 20400.0,
                    'valueInRule' => true,
                    'ruleValue' => 177480.0,
                    'aggregatedPlusCurrent' => 177480.0 + 20400.0
                ]
            ]
        ], $presenter->present(700000));
    }

    protected function setUp(): void
    {
        parent::setUp();
        $scale = new Scale(new MultiplicationOperator);
        $scale->addScaleRule(ScaleRule::make(0, 2));
        $scale->addScaleRule(ScaleRule::make(2, 4));
        $scale->addScaleRule(ScaleRule::make(1, 8));
        $this->presenter = new ScalePresenter($scale);
    }
}
