<?php

namespace Laudis\Scale\Testing;

use DateTime;
use Laudis\Scale\ContextualScaleRepository;
use Laudis\Scale\Contracts\ScaleInterface;
use Laudis\Scale\Operators\MultiplicationOperator;
use Laudis\Scale\Scale;
use PHPUnit\Framework\TestCase;

class ContextualScaleRepositoryTest extends TestCase
{
    /** @var ContextualScaleRepository */
    private $repo;
    /** @var ScaleInterface */
    private $scale1, $scale2, $scale3;

    public function testGetFromDate(): void
    {
        $scale = $this->repo->getFromDate(DateTime::createFromFormat('Y-m-d', '2001-01-01'), 'scale');
        self::assertSame($this->scale1, $scale);
        $scale = $this->repo->getFromDate(DateTime::createFromFormat('Y-m-d', '1951-01-01'), 'scale');
        self::assertSame($this->scale2, $scale);
        $scale = $this->repo->getFromDate(DateTime::createFromFormat('Y-m-d', '2151-01-01'), 'scale');
        self::assertSame($this->scale3, $scale);
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->repo = new ContextualScaleRepository();
        $this->repo->addScale('scale', DateTime::createFromFormat('Y-m-d', '2000-01-01'),
            $this->scale1 = $this->makeScale());
        $this->repo->addScale('scale', DateTime::createFromFormat('Y-m-d', '1950-01-01'),
            $this->scale2 = $this->makeScale());
        $this->repo->addScale('scale', DateTime::createFromFormat('Y-m-d', '2150-01-01'),
            $this->scale3 = $this->makeScale());
    }

    private function makeScale(): ScaleInterface
    {
        return new Scale(new MultiplicationOperator);
    }
}
