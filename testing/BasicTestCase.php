<?php

namespace Laudis\Scale\Testing;

use Laudis\Scale\Operators\MultiplicationOperator;
use Laudis\Scale\Scale;
use Laudis\Scale\ScaleRule;
use const PHP_FLOAT_MAX;
use PHPUnit\Framework\TestCase;

/**
 * Class BasicTestCase
 * @package Laudis\Scale\Testing
 */
final class BasicTestCase extends TestCase
{
    /** @var Scale */
    private $scale;

    protected function setUp(): void
    {
        parent::setUp();
        $this->scale = new Scale(new MultiplicationOperator());
        $this->scale->addScaleRule(ScaleRule::make(0, 0.2498));
        $this->scale->addScaleRule(ScaleRule::make(25000, 0.3193));
        $this->scale->addScaleRule(ScaleRule::make(90000, 0.3554));
        $this->scale->addScaleRule(ScaleRule::make(322500, 0));
    }

    public function testBeginFirstRule(): void
    {
        $this->assertEquals($this->scale->calculate(0), 0);
    }

    public function testEndFirstRule(): void
    {
        $this->assertEquals(6245, $this->scale->calculate(25000));
    }

    public function testEndMiddleRule(): void
    {
        $this->assertEquals(26999.5, $this->scale->calculate(90000));
    }

    public function testEndLastRule(): void
    {
        $this->assertEquals(109630, $this->scale->calculate(322500));
    }

    public function testBeyondLastRule(): void
    {
        $this->assertEquals(109630, $this->scale->calculate(PHP_FLOAT_MAX));
    }
}
